﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Objects/WeaponObject", order = 2)]
public class WeaponData : ScriptableObject
{
    public string WeaponName = "Weapon name";
    public int WeaponDamage = 1;
    public int WeaponRange = 1;
    public WeaponType WeaponType;
    public float FireRate = 1f;
    public GameObject Bullet;
    public float BulletSpeed = 1f;
}
