﻿using UnityEngine;

public interface IPlayer
{
    void RegisterPlayer();
    void UnRegisterPlayer();

    Transform GetTransform();
}
