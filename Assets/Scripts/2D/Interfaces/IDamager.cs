﻿public interface IDamager
{    
    int Damage { get; }
    bool IsStartAttack { get; }
    void SetDamage();
}
