﻿public interface IDamaged
{
    int Health { get; set; }
    void TakeDamage(int damage);
    void Die();
}
