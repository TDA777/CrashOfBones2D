﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static float HorizontalAxis;
    public static Action<float> JumpAction;
    public static Action<string> FireAction;

    private float jumpTimer;
    private Coroutine WaitJumpCoroutine;


    void Start()
    {
        HorizontalAxis = 0f;
    }

    private void OnDestroy()
    {
        HorizontalAxis = 0f;
    }

    void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump")) 
        {
            if (WaitJumpCoroutine == null)
            {
                WaitJumpCoroutine = StartCoroutine(WaitJump());
            }
            jumpTimer = Time.time;
        }
        if (Input.GetButtonDown("Fire1"))
        {
            if (FireAction != null)
            {
                FireAction.Invoke("Fire1");
            }
        }
        if (Input.GetButtonDown("Fire2"))
        {
            if (FireAction != null)
            {
                FireAction.Invoke("Fire2");
            }
        }

    }

    private IEnumerator WaitJump()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        if(JumpAction != null)
        {
            float forse = Time.time - jumpTimer < 0.2f ? 1.25f : 1f;
            JumpAction.Invoke(forse) ;
        }
        WaitJumpCoroutine = null;
    }
}
