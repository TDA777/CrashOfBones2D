using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBackGroundControl : MonoBehaviour {

    public static Action OnBackGroundHide;
    public static LoadingBackGroundControl instance;
    private Image backgound;
    private Text backgoundText;
    private float speedAlpha = 0.005f;
	void Start ()
    {
        instance = this;
        backgound = GetComponent<Image>();
        backgoundText = (transform.GetChild(0)).GetComponent<Text>();
    }
	
	public void Hide()
    {
        StartCoroutine(Transparensy());
    }

    private IEnumerator Transparensy()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (backgound.color.a < 0.05f)
            {
                if(OnBackGroundHide !=null){ OnBackGroundHide(); }
                yield break;
            }
            else
            {
                float alpha = backgound.color.a - speedAlpha;
                print(alpha);
                backgound.color = new Color(backgound.color.r, backgound.color.g, backgound.color.b, alpha);
                backgoundText.color = new Color(backgound.color.r, backgound.color.g, backgound.color.b, alpha);
            }
        }
    }
}
