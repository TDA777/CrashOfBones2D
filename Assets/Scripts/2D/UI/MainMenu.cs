﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    void Start()
    {
        GameManager.SetGameState(GameState.MainMenu);
    }

    public void LoadLevel(int LevelNm)
    {
        SceneLoader.LoadLevel(new NextLevelData("", true, LevelNm));
    }
}
