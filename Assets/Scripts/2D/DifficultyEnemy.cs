﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Objects/DifficultyEnemy", order = 3)]
public class DifficultyEnemy : ScriptableObject
{
    public float Speed = 3;
    public int MultiplyDamage = 4;
    public int Health = 1;
}
