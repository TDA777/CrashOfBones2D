﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stun : MonoBehaviour
{
    private CharacterMovement characterMovement;
    private StunPoint stunPoint;
    private void Start()
    {        
        stunPoint = GetComponent<StunPoint>();
        characterMovement = GetComponent<CharacterMovement>();
        if (stunPoint)
        {
            (characterMovement as PlayerMovement).enabled = false;
            stunPoint.SetSunEffect();
            StartCoroutine(StopStun());
        }
    }

    private IEnumerator StopStun()
    {
        yield return new WaitForSeconds(2f);
        if(characterMovement)
            characterMovement.enabled = true;
        if (stunPoint)
            stunPoint.DestroySunEffect();
        Destroy(this);
        yield return null;
    }
}
