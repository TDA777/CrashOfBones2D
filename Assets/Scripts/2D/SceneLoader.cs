using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static NextLevelData NextLevel;

    private void Awake()
    {
        LoadingBackGroundControl.OnBackGroundHide += (() => { SceneManager.UnloadSceneAsync("LoadingScene"); });
    }
    private void OnDestroy()
    {
        LoadingBackGroundControl.OnBackGroundHide -= (() => { SceneManager.UnloadSceneAsync("LoadingScene"); });
    }

    public static void LoadLevel(NextLevelData nextLevel)
    {
        NextLevel = nextLevel;
        SceneManager.LoadScene("LoadingScene");
    }

    IEnumerator Start()
    {
        GameManager.SetGameState(GameState.Loading);
        yield return new WaitForSecondsRealtime(1f);
        if (NextLevel == null)
        {
            SceneManager.LoadScene("MainMenu");
            yield break;
        }
        AsyncOperation loading = null;
        bool isLevelName = !string.IsNullOrEmpty(NextLevel.LevelName);
        if (isLevelName)
        {
            if (!NextLevel.AsyncLoading)
            {
                SceneManager.LoadScene(NextLevel.LevelName);
                yield break;
            }

            loading = SceneManager.LoadSceneAsync(NextLevel.LevelName, LoadSceneMode.Additive);
        }
        else
        {
            if (!NextLevel.AsyncLoading)
            {
                SceneManager.LoadScene(NextLevel.LevelNumber);
                yield break;
            }

            loading = SceneManager.LoadSceneAsync(NextLevel.LevelNumber, LoadSceneMode.Additive);
        }

        while (!loading.isDone)
        {
            LoadingBackGroundControl.instance.Hide();
            yield return null;
        }
    }

    void Update()
    {

    }
}

public class NextLevelData
{
    public string LevelName;
    public int LevelNumber;
    public bool AsyncLoading;

    public NextLevelData(string Name, bool IsAsyc, int Number = -1)
    {
        LevelName = Name;
        AsyncLoading = IsAsyc;
        LevelNumber = Number;

    }
}
