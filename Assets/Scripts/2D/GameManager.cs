﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { MainMenu = 0, Loading =1, Game =2, GamePause = 3}

public class GameManager : MonoBehaviour
{
    private static GameState currentGameState;
    public static Action<GameState> Action_GameState;

    public static GameState GetGameState()
    {
        return currentGameState;
    }

    public static void SetGameState(GameState NewState)
    {
        currentGameState = NewState;
        if (Action_GameState != null)
        {
            Action_GameState(currentGameState);
        }
    }




}
