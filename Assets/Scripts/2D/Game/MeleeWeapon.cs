﻿using UnityEngine;

public class MeleeWeapon : MonoBehaviour, IDamager
{
    [SerializeField]
    private WeaponData m_WeaponData;
    [SerializeField]
    private Transform m_ShootPoint;
    [SerializeField]
    private GameObject m_DamageEffect;
    [SerializeField]
    private Transform m_EffectPoint;



    private float reloadTimer;

    public int Damage
    {
        get { return m_WeaponData.WeaponDamage; }
    }

    public bool IsStartAttack
    {
        get 
        {
            if(Time.time >= reloadTimer)
            {
                reloadTimer = Time.time + m_WeaponData.FireRate;
                return true;
            }
            return false;
        }
    }

    public IDamaged GetTarget()
    {
        IDamaged target = null;
        RaycastHit2D hit = Physics2D.Raycast(m_ShootPoint.position, m_ShootPoint.right, m_WeaponData.WeaponRange);
        if (hit.collider != null)
        {
            target = hit.transform.root.gameObject.GetComponent<IDamaged>();
        }
        return target;
    }

    public void SetDamage()
    {
        IDamaged target = GetTarget();
        if(target != null)
        {
            if (m_DamageEffect && m_EffectPoint)
            {
                GameObject obj = Instantiate(m_DamageEffect, m_EffectPoint.position,m_EffectPoint.rotation);
                Destroy(obj, 1f);
            }
            target.TakeDamage(Damage);
        }
    }
}
