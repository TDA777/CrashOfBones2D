﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterMovement : MonoBehaviour {

    public bool isFreezing;
    public abstract void Move(Vector2 direction);
    public abstract void Jump(float Force);

}
