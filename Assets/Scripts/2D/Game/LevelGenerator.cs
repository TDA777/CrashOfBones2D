﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    private enum RepeateType
    {
        Nome,Round,Random
    }
    [Header("BaseSettings")]
    [SerializeField]
    private RepeateType m_RepeateType;

    [SerializeField]
    private GameObject m_StartChunk;

    [System.Serializable]
    private struct ChunkQueue
    {
        public GameObject[] Chunks;
        public GameObject EndChunk;
        [Range(0, 100)]
        public int MaxChunksCount;

    }
    [Header("Side Settings")]
    [SerializeField]
    private ChunkQueue m_LeftSideQueue;
    [Space(5)]
    [SerializeField]
    private ChunkQueue m_RightSideQueue;

    private GameObject lastGeneratedChunkObject;

    private void Start()
    {
        GameObject firstChunk = GenerateChunk(m_StartChunk, Vector3.zero, true);

        Vector3 playerStartPosition = Vector3.zero;
        foreach (Transform tranf  in firstChunk.transform.GetComponentInChildren<Transform>())
        {
            if (tranf.gameObject.name == "firstChunk")
            {
                playerStartPosition = tranf.position;
                break;
            }
        }

        SceneManagerScript sceneManagerScript = FindObjectOfType<SceneManagerScript>();
        sceneManagerScript.Player.GetTransform().position = playerStartPosition;
        lastGeneratedChunkObject = firstChunk;
        GenerateLevelSide(m_RightSideQueue, true);
        lastGeneratedChunkObject = firstChunk;
        GenerateLevelSide(m_LeftSideQueue, false);

    }

    private void GenerateLevelSide(ChunkQueue queue, bool isRigth)
    {
        int dir = isRigth ? 1 : -1;
        int counter = queue.Chunks.Length + queue.MaxChunksCount;
        if (m_RepeateType == RepeateType.Nome)
        {
            counter = queue.Chunks.Length;
        }
        if (m_RepeateType == RepeateType.Random)
        {
            counter = queue.MaxChunksCount;
        }
        if (queue.Chunks.Length > 0)
        {
            for (int i = 0; i < counter; i++)
            {
                if (i > 1)
                {
                    lastGeneratedChunkObject.SetActive(false);
                }

                int nm = i;

                if (m_RepeateType == RepeateType.Random)
                {
                    nm = Random.Range(0, queue.Chunks.Length);
                }
                else if (m_RepeateType == RepeateType.Round)
                {/*
                    int repeatCicle = i / queue.Chunks.Length;
                    nm = i - repeatCicle * queue.Chunks.Length;*/
                    nm = i % queue.Chunks.Length;
                }

                float posX = (i + 1) * dir * 20f;//TODO
                Vector3 pos = new Vector3(posX, 0f, 0f);
                lastGeneratedChunkObject = GenerateChunk(queue.Chunks[nm], pos, isRigth);
            }
            lastGeneratedChunkObject.SetActive(false);
        }

        if (queue.EndChunk)
        {
            float posX = (counter + 1) * dir * 20f;//TODO
            Vector3 pos = new Vector3(posX, 0f, 0f);
            lastGeneratedChunkObject = GenerateChunk(queue.EndChunk, pos, isRigth);
            lastGeneratedChunkObject.SetActive(queue.Chunks.Length == 0);
        }

    }

    private GameObject GenerateChunk(GameObject chunk,Vector3 pos, bool isRigth)
    {
        GameObject newChunk = Instantiate(chunk, pos, Quaternion.identity);

        GameObject obj = new GameObject("Level Chunk Trigger");
        obj.transform.parent = newChunk.transform;
        obj.transform.localPosition = new Vector3(10f, 0, 0);
        Rigidbody2D rig = obj.AddComponent<Rigidbody2D>();
        rig.bodyType = RigidbodyType2D.Kinematic;
        BoxCollider2D boxCollider2D = obj.AddComponent<BoxCollider2D>();
        boxCollider2D.size = new Vector2(2f, 20f);
        boxCollider2D.isTrigger = true;

        LevelChunk newLeveltrigger = obj.AddComponent<LevelChunk>();
        newLeveltrigger.ChunkRoot = newChunk;


        LevelChunk lastGeneratedLevelTrigger = GetLevelChunk(lastGeneratedChunkObject);
        

        if (lastGeneratedLevelTrigger)
        {
            if (isRigth)
            {
                lastGeneratedLevelTrigger.RightChunk = newLeveltrigger;
                newLeveltrigger.LeftChunk = lastGeneratedLevelTrigger;
            }
            else
            {
                lastGeneratedLevelTrigger.LeftChunk = newLeveltrigger;
                newLeveltrigger.RightChunk = lastGeneratedLevelTrigger;
            }
        }
        return newChunk;
    }

    private LevelChunk GetLevelChunk(GameObject obj)
    {
        if (!obj)
        {
            return null;
        }
        foreach (Transform transform in obj.transform)
        {
            LevelChunk levelChunk = transform.GetComponent<LevelChunk>();
            if (levelChunk)
            {
                return levelChunk;
            }
        }
        return null;
    }
}
