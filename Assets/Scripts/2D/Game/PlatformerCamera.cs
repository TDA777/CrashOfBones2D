﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerCamera : MonoBehaviour
{
    private SceneManagerScript SceneManager;
    [SerializeField]
    private float m_HorizontalMin = 2f;
    [Range(0.1f,10f)]
    [SerializeField]
    private float m_HorizontalMax = 2f;



    private void Start()
    {
        SceneManager = FindObjectOfType<SceneManagerScript>();
    }

    private void Update()
    {
        if (SceneManager.Player == null)
        {
            return;
        }

        Vector3 playerPosition = SceneManager.Player.GetTransform().position;
        Vector3 cameraPosition = transform.position;

        float xDisctance = Mathf.Abs(playerPosition.x - cameraPosition.x);
        if (xDisctance > m_HorizontalMin)
        {
            float x = cameraPosition.x;
            float k = xDisctance / m_HorizontalMax * Time.deltaTime;

            x = Mathf.Lerp(x, playerPosition.x, k);
            cameraPosition.x = x;
            transform.position = cameraPosition;
        }
    }
}
