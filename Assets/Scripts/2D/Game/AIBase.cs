﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBase : AIBehaviour
{
    public override void StartBehaviour(AIObject target)
    {
        StartCoroutine(UpdateBehaviour(target.Weapon, target.Transform));
    }

    private IEnumerator UpdateBehaviour(EnemyWeapon weapon, Transform transform)
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);//TODO - move to settings

            var hit = Physics2D.Raycast(transform.position, transform.right, 1f);

            if (hit.collider != null)
            {
                if (hit.transform.GetComponent<IPlayer>() != null)
                {
                    weapon.Attack();
                }
            }
        }
    }
}
