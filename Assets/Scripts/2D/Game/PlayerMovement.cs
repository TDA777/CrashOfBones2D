﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : CharacterMovement
{
    [SerializeField]
    private float m_MaxSpeed = 10f;
    [SerializeField]
    private float m_jumpForce = 5f;
    [SerializeField]
    private Animator m_Animator;



    [SerializeField]
    private Transform m_Graphics;

    private Rigidbody2D Rig;

    private void Start()
    {
        Rig = GetComponent<Rigidbody2D>();
        InputController.JumpAction += JumpFromAction;
    }
    private void OnDestroy()
    {
        InputController.JumpAction -= JumpFromAction;
    }

    private void OnDisable()
    {
        Vector2 velocity = Rig.velocity;
        velocity.x = 0;
        Rig.velocity = velocity;
        m_Animator.SetFloat("Speed", 0);
    }

    private void FixedUpdate()
    {
        if (isFreezing)
        {
            Vector2 velocity = Rig.velocity;
            velocity.x = 0;
            Rig.velocity = velocity; m_Animator.SetFloat("Speed", 0);
            return;
        }


        Vector2 direction = new Vector2(InputController.HorizontalAxis, 0f);
        if (!IsGrounded())
        {
            direction.x *= 0.5f;
        }
        Move(direction);
    }


    private void Update()
    {
        if (IsGrounded())
        {
            m_Animator.SetFloat("Speed",Mathf.Abs(Rig.velocity.x));
        }
        else
        {
            m_Animator.SetFloat("Speed", 0);
        }
        if (Mathf.Abs(Rig.velocity.x)<0.01f)//TODO move game constants
        {
            return;
        }

        float yAngle = Rig.velocity.x > 0 ? 180f : 0f;
        m_Graphics.localEulerAngles = new Vector3(0, yAngle, 0);
    }

    public override void Move(Vector2 direction)
    {
        Vector2 velocity = Rig.velocity;
        velocity.x = direction.x * m_MaxSpeed;
        Rig.velocity = velocity;
    }

    public override void Jump(float Force)
    {
        Rig.AddForce(new Vector2(0, Force), ForceMode2D.Impulse);
    }


   private void JumpFromAction(float forceMulyplayer)
    {
        if (IsGrounded())
        {
            Jump(m_jumpForce * forceMulyplayer);
        }
    }

    private bool IsGrounded()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 0.2f);
        return hit.collider != null;
    }

}
