﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : MonoBehaviour,IDamaged
{
    [SerializeField]
    private LevelObjectData m_ObjectData;

    Rigidbody2D rig;

    private int health;
    public int Health
    {
        get { return health; }
        set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        Destroy(gameObject);
    }
    [ContextMenu("Rename")]
    private void Rename()
    {
        if (m_ObjectData)
        {
            gameObject.name = m_ObjectData.Name;
        }
    }

    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        rig.isKinematic = m_ObjectData.IsStatic;
        Health = m_ObjectData.Health;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
