﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunPoint : MonoBehaviour
{
    [SerializeField]
    private Transform m_StunPoint;

    [SerializeField]
    private GameObject m_StunEffect;
    private GameObject stunEffect;

    public void SetSunEffect()
    {
        stunEffect = Instantiate(m_StunEffect, m_StunPoint.position, m_StunPoint.rotation);
        stunEffect.transform.parent = m_StunPoint.transform;
    }
    public void DestroySunEffect()
    {
        Destroy(stunEffect);
    }
}
