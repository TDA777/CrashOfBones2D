﻿using UnityEngine;

public abstract class AIBehaviour : MonoBehaviour
{
    public abstract void StartBehaviour(AIObject target);

}
