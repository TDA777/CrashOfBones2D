﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    object block;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamaged target = collision.gameObject.GetComponent<IDamaged>();
        if (target != null)
        {
            Rigidbody2D rig = collision.gameObject.GetComponent<Rigidbody2D>();
            if (rig)
            {
                rig.isKinematic = true;
            }
            target.Health = 0;
        }
    }
}
