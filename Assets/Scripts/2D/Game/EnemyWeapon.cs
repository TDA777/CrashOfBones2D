﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Weapon;
    private IDamager weapon;

    [SerializeField]
    private Animator m_Animator;

    private void Start()
    {
        weapon = m_Weapon.GetComponent<IDamager>();
    }
    public void Attack()
    {
        if (weapon.IsStartAttack)
        {
            m_Animator.SetTrigger("Attack");
        }
    }

    public void Hit()
    {
        if (weapon != null)
        {
            weapon.SetDamage();
        }
    }
}
