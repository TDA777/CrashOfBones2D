﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    [SerializeField]
    private GameObject m_MeleeWeaponObject;
    private IDamager meleeWeapon;


    [SerializeField]
    private GameObject m_RangeWeaponObject;
    public IDamager rangeWeapon;

    [SerializeField]
    private Animator m_Animator;
    [SerializeField]
    private PlayerMovement m_PlayerMovement;

    private IDamager damager;

    void Start()
    {
        InputController.FireAction += Attack;
        meleeWeapon = m_MeleeWeaponObject.GetComponent<IDamager>();
        rangeWeapon = m_RangeWeaponObject.GetComponent<IDamager>();
        print(rangeWeapon);
    }
    private void OnDestroy()
    {
        InputController.FireAction -= Attack;
    }

    private void Attack(string type)
    {
       
        switch (type)
        {
            case "Fire1":
                if (meleeWeapon.IsStartAttack)
                {
                    damager = meleeWeapon;
                    m_Animator.SetTrigger(type);
                }
                break;
            case "Fire2":
                if (rangeWeapon.IsStartAttack)
                {
                    damager = rangeWeapon;
                    m_Animator.SetTrigger(type);
                    m_PlayerMovement.isFreezing = true;
                }
                break;
            default:
                Debug.Log("Player Attack of type ["+type+"] not set");
                break;
        }
    }

    public void Hit()
    {
        if (damager != null)
        {
            damager.SetDamage();
            damager = null;
            m_PlayerMovement.isFreezing = false;
        }
    }
}
