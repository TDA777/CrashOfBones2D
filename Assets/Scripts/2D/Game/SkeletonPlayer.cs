﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonPlayer : MonoBehaviour, IPlayer,IDamaged
{
    [SerializeField]
    private int health = 3;
    [SerializeField]
    private Animator m_Animator;

    public int Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    public Transform GetTransform()
    {
        return transform;
    }

    public void UnRegisterPlayer()
    {

        SceneManagerScript sceneManager = FindObjectOfType<SceneManagerScript>();
        sceneManager.Player = null;
    }
    public void Die()
    {
        m_Animator.SetTrigger("Die");
        print("Die()");
        Destroy(GetComponent<PlayerMovement>());
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(this);
    }

    public void RegisterPlayer()
    {
        //TODO
        SceneManagerScript sceneManager = FindObjectOfType<SceneManagerScript>();
        if (sceneManager.Player == null)
        {
            sceneManager.Player = this;
        }else
        {
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(int damage)
    {

        print("Health " + Health + "-damage" + damage);
        if (damage > 0 && (Health - damage) > 0)
        {
            m_Animator.SetTrigger("Hit");
        }
        Health -= damage;
    }


    void Awake()
    {
        RegisterPlayer();
    }

    void Update()
    {

    }
}
