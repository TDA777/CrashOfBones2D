﻿using UnityEngine;

public class PlayerBullet : MonoBehaviour,IDamager
{
    private int damage;
    public int Damage
    {
        get
        {
            return damage;
        }
    }

    public void SetDamage(int val)
    {
        damage = val;
    }

    private IDamaged target;

    public bool IsStartAttack
    {
        get
        {
            if (target == null)
            {
                return false;
            }
            if (target as SkeletonPlayer)
            {
                return false;
            }
            return true;
        }
    }

    public void SetDamage()
    {
        if (IsStartAttack)
        {
            target.TakeDamage(Damage);
            Destroy(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        target = collision.transform.root.GetComponent<IDamaged>();
        SetDamage();
    }
}
