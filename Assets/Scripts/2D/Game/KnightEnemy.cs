﻿using UnityEngine;

public class KnightEnemy : MonoBehaviour, IEnemy, IDamaged
{
    [SerializeField]
    private AIBehaviour[] m_AIBehaviours;
    [SerializeField]
    private DifficultyEnemy m_DifficultyEnemy;
    
    private int health = 1;
    [SerializeField]
    private EnemyWeapon m_Weapon;
    [SerializeField]
    private Transform m_Forward;
    private int currDamageMult;

    public int Health
    {
        get { return health; }
        set 
        {
            health = value;
            if (health <= 0 )
            {
                Die();
            }
        }
    }

    void Awake()
    {
        RegisterEnemy();
    }

  

    public void RegisterEnemy()
    {
        //TODO
        SceneManagerScript sceneManager = FindObjectOfType<SceneManagerScript>();
        sceneManager.Enemies.Add(this);
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public void TakeDamage(int damage)
    {
        Health -= damage + currDamageMult;
    }

    private void Start()
    {
        gameObject.GetComponent<EnemyMovementScript>().m_MaxSpeed = m_DifficultyEnemy.Speed;
        health = m_DifficultyEnemy.Health;
        currDamageMult = m_DifficultyEnemy.MultiplyDamage;

        AIObject target = new AIObject();
        target.GameObject = gameObject;
        target.Transform = m_Forward;
        target.MoveMent = GetComponent<CharacterMovement>();
        target.Weapon = m_Weapon;

        foreach (var behaviour in m_AIBehaviours)
        {
            behaviour.StartBehaviour(target);
        }
    }

}
