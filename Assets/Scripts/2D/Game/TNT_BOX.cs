﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TNT_BOX : MonoBehaviour,IDamaged
{
    [SerializeField]
    private LevelObjectData m_ObjectData;
    [SerializeField]
    private GameObject m_Explosion;
    [SerializeField]
    private float m_ExplosionRadius = 2f;
    [SerializeField]
    private int m_Damage = 1;
    [SerializeField]
    private Transform m_ExplosionPoint;

    
    private int health = 1;
    public int Health
    {
        get { return health; }
        set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    public void TakeDamage(int damage)
    {
        Health = 0;
    }

    public void Die()
    {
        GameObject obj = Instantiate(m_Explosion, m_ExplosionPoint.position, m_ExplosionPoint.rotation);
        Destroy(this);
        Destroy(GetComponent<Collider2D>());
        Destroy(gameObject,0.2f); 

        Destroy(obj, 1f);
        Collider2D[] collider = Physics2D.OverlapCircleAll(m_ExplosionPoint.position,m_ExplosionRadius);
        List<Transform> wasDamaged = new List<Transform>();
        foreach (var item in collider)
        {
            Transform root = item.transform.root;
            var damaged = root.GetComponent<IDamaged>();
            if (damaged != null&& !wasDamaged.Contains(root))
            {
                wasDamaged.Add(root);
                damaged.TakeDamage(m_Damage);
            }

            if (root.GetComponent<StunPoint>() && !root.GetComponent<Stun>())
            {
                if (root.GetComponent<IDamaged>() != null)
                {
                    root.gameObject.AddComponent<Stun>();
                }
            }
        }
    }
    [ContextMenu("Rename")]
    private void Rename()
    {
        if (m_ObjectData)
        {
            gameObject.name = m_ObjectData.Name;
        }
    }
    
    void Start()
    {
        Health = m_ObjectData.Health;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Health > 0 && collision.gameObject.GetComponent<Rigidbody2D>())
        {
            Health = 0;
        }
    }
}
