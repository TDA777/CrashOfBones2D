﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsCreator : MonoBehaviour
{
    [SerializeField]
    private bool m_IsRandom;
    [SerializeField]
    private Transform m_SpawnPoint;
    [SerializeField]
    private float[] m_SpawnTimeMap;
    [SerializeField]
    private GameObject[] m_SpawnObjects;
    [SerializeField]
    private bool m_IsRepeate;



	void Start ()
    {
        StartCoroutine(SpawnCoroutine());

	}
	
	private IEnumerator SpawnCoroutine()
    {
        int timeMapCounter = 0;
        int objectsCounter = 0;

        while (timeMapCounter < m_SpawnObjects.Length)
        {
            yield return new WaitForSeconds(m_SpawnTimeMap[timeMapCounter]);
            if (m_IsRandom)
            {
                objectsCounter = Random.Range(0,m_SpawnObjects.Length);

            }
            else if (objectsCounter == m_SpawnObjects.Length)
            {
                objectsCounter = 0;
            }


            GameObject obj = Instantiate(m_SpawnObjects[objectsCounter], m_SpawnPoint.position, m_SpawnPoint.rotation);
            timeMapCounter++;
            objectsCounter++;
        }
        if (m_IsRepeate)
        {
            StartCoroutine(SpawnCoroutine());
        }
    }
}
