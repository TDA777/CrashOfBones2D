﻿using UnityEngine;

public class DistanceWeapon : MonoBehaviour, IDamager
{
    [SerializeField]
    private WeaponData m_WeaponData;
    [SerializeField]
    private Transform m_ShootPoint;
    private float reloadTimer;

    public int Damage
    {
        get { return m_WeaponData.WeaponDamage; }
    }

    public bool IsStartAttack
    {
        get
        {
            if (Time.time >= reloadTimer)
            {
                reloadTimer = Time.time + m_WeaponData.FireRate;
                return true;
            }
            return false;
        }
    }

    public void SetDamage()
    {
        if(!m_WeaponData.Bullet)
        {
            Debug.LogError("Player Weapon bullet not set");
            return;
        }
        GameObject bullet = Instantiate(m_WeaponData.Bullet, m_ShootPoint.position, m_ShootPoint.rotation);
        bullet.SendMessage("SetDamage", m_WeaponData.WeaponDamage);

        Rigidbody2D rig = bullet.GetComponent<Rigidbody2D>();
        rig.AddForce(m_ShootPoint.transform.right * m_WeaponData.BulletSpeed,ForceMode2D.Impulse);
        Destroy(bullet, 5f);

    }
}
