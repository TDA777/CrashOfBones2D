﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct AIObject
{
    public GameObject GameObject;
    public Transform Transform;
    public EnemyWeapon Weapon;
    public CharacterMovement MoveMent;
}
