﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyMovementScript : CharacterMovement
{
    public float m_MaxSpeed = 10f;
    [SerializeField]
    private Transform m_Graphics;
    [SerializeField]
    private Animator m_Animator;

    private Rigidbody2D Rigidbody;


    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
    }

    public override void Jump(float Force)
    {
    }

    public override void Move(Vector2 Direction)
    {
        var velocity = Rigidbody.velocity;
        velocity.x = Direction.x * m_MaxSpeed;
        Rigidbody.velocity = velocity;

        m_Animator.SetFloat("Speed", Mathf.Abs(Direction.x));

        if (Mathf.Abs(Direction.x) < 0.01f)
        {
            return;
        }
        float xAngle = Direction.x > 0 ? 180f : 0f;
        m_Graphics.localEulerAngles = new Vector3(0, xAngle, 0);
    }
}
