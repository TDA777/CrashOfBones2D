﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChunk : MonoBehaviour
{
    [HideInInspector]
    public LevelChunk RightChunk;
    [HideInInspector]
    public LevelChunk LeftChunk;

    public GameObject ChunkRoot;

    public void EnebleNextChhhhunk(bool isRight, bool enable)
    {
        LevelChunk chunk = isRight ? RightChunk : LeftChunk;

        if (chunk)
        {
            chunk.ChunkRoot.SetActive(enable);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.transform.root.GetComponent<IPlayer>();

        if (player == null)
        {
            return;
        }

        var isToRight = player.GetTransform().position.x < transform.position.x;
        var chunk = isToRight ? RightChunk : this;

        if (chunk)
        {
            chunk.EnebleNextChhhhunk(isToRight, true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var player = collision.transform.root.GetComponent<IPlayer>();

        if (player == null)
        {
            return;
        }

        var isToLeft = player.GetTransform().position.x > transform.position.x;
        var chunk = isToLeft ? this : RightChunk;

        if (chunk)
        {
            chunk.EnebleNextChhhhunk(!isToLeft, false);
        }
    }
    
}
