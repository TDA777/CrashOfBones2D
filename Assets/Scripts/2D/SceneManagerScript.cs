﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerScript : MonoBehaviour
{
    [HideInInspector]
    public IPlayer Player;
    [HideInInspector]
    public List<IEnemy> Enemies = new List<IEnemy>();


    void Start()
    {
        GameManager.SetGameState(GameState.Game);
        print(Player);
        print(Enemies.Count + " Enemies");
    }
}
