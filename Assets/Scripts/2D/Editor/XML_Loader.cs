using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using UnityEditor;
using System.Reflection;
using System;
using System.Xml.Serialization;

public class XML_Loader : MonoBehaviour
{

    public static float OffsetPivotY;
    public static float OffsetPivotX;
    [MenuItem("Tools/LoadXML")]
    public static void LoadXML()
    {
        UnityEngine.Object[] selection = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
        if (selection.Length < 1)
        {
            Debug.LogWarning("Selection empty");
        }
        foreach (var item in selection)
        {
            string imagePath = AssetDatabase.GetAssetPath(item);
            string xmlPath = imagePath.Split('.')[0];
            xmlPath = xmlPath.Remove(0, 7);//Remove "Assets/" from start path
            xmlPath = Path.Combine(Application.dataPath, xmlPath) + ".xml";
            xmlPath = xmlPath.Replace('\\', '/');

            SetSprites(imagePath, xmlPath);
        }
    }

    public static void LoadXML_BySize(string imagePath, Vector2 frame, Vector2 pivot,bool fitBySize = true)
    {
        Texture2D texture2D = LoadTexture(imagePath);

        TextureAtlas import = new TextureAtlas();

        TextureImporter ti = AssetImporter.GetAtPath(imagePath) as TextureImporter;
        ti.isReadable = true;
        ti.spriteImportMode = SpriteImportMode.Multiple;

        List<SpriteMetaData> newData = new List<SpriteMetaData>();
        int widthTexture = texture2D.width;
        int heightTexture = texture2D.height;

        float countWidth, countHeight;
        countWidth = (float)Math.Floor(widthTexture / frame.x);
        countHeight = (float)Math.Floor(heightTexture / frame.y);
        if (fitBySize)
        {
            countWidth = (float)Math.Floor(widthTexture / frame.x);
            countHeight = (float)Math.Floor(heightTexture / frame.y);
            frame = new Vector2(widthTexture/ countWidth, heightTexture/ countHeight);
        }
        else
        {
            countWidth = (float)Math.Ceiling(widthTexture / frame.x);
            countHeight = (float)Math.Ceiling(heightTexture / frame.y);
        }
        float curWidth = 0, curHeigth = 0;
        for (int i = 0; i < countWidth; i++)
        {
            for (int j = 0; j < countHeight; j++)
            {
                SpriteMetaData smd = new SpriteMetaData();

                curWidth = i * frame.x;
                curHeigth = j * frame.y;
                smd.alignment = (int)SpriteAlignment.Custom;
                smd.pivot = pivot;
                smd.name = "image_" + i + "_" + j;
                smd.rect = new Rect(curWidth, curHeigth, frame.x, frame.y);
                newData.Add(smd);

            }
        }

        ti.spritesheet = newData.ToArray();
        AssetDatabase.ImportAsset(imagePath, ImportAssetOptions.ForceUpdate);

    }
    private static void SetSprites(string imagePath, string xmlPath)
    {
        if(!File.Exists(imagePath))
        {
            Debug.LogError("Image file not founded");
            return;
        }

        if(!File.Exists(xmlPath))
        {
            Debug.LogError("XML file not founded");
            return;
        }

        Texture2D texture2D = LoadTexture(imagePath);

        TextureAtlas import = new TextureAtlas();
        XmlSerializer formatter = new XmlSerializer(typeof(TextureAtlas)); using (FileStream fs = new FileStream(xmlPath, FileMode.Open))
        {
            import = formatter.Deserialize(fs) as TextureAtlas;
        }
        
        TextureImporter ti = AssetImporter.GetAtPath(imagePath) as TextureImporter;
        ti.isReadable = true;
        if ( ti.spriteImportMode == SpriteImportMode.Multiple)
        {
            ti.spriteImportMode = SpriteImportMode.Single;
            ti.SaveAndReimport();
        }
        ti.spriteImportMode = SpriteImportMode.Multiple;

        List<SpriteMetaData> newData = new List<SpriteMetaData>();
        int widthTexture = texture2D.width;
        int heightTexture = texture2D.height;
        for (int i = 0; i < import.items.Length; i++)
        {
            SubTexture subTex = import.items[i];

            SpriteMetaData smd = new SpriteMetaData();
            float sizeLeftToPoint = subTex.frameWidth/*251*/ * OffsetPivotX;
            float sizeTopToPoint = subTex.frameHeight/*215*/ * OffsetPivotY;

            float sizeLeftSum = sizeLeftToPoint + subTex.frameX/*-9*/;
            float sizeTopSum = sizeTopToPoint + subTex.frameY/*-24*/;

            float x = sizeLeftSum / subTex.width/*208*/;
            float y = 1 - sizeTopSum / subTex.height/*177*/;

            smd.alignment = (int)SpriteAlignment.Custom;
            smd.pivot = new Vector2(x, y);
            smd.name = subTex.name;
            smd.rect = new Rect(subTex.x, heightTexture - subTex.y - subTex.height, subTex.width, subTex.height);
            newData.Add(smd);
        }

        ti.spritesheet = newData.ToArray();
        AssetDatabase.ImportAsset(imagePath, ImportAssetOptions.ForceUpdate);
    }


    private static Texture2D LoadTexture(string FilePath)
    {
        Texture2D Tex2D;
        byte[] FileData;

        if (File.Exists(FilePath))
        {
            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2);              // Create new "empty" texture
            if (Tex2D.LoadImage(FileData))           // Load the imagedata into the texture (size is set automatically)
                return Tex2D;                       // If data = readable -> return texture
        }
        return null;                              // Return null if load failed
    }

    [XmlRoot(ElementName = "TextureAtlas")]
    public class TextureAtlas
    {
        [XmlAttribute]
        public string imagePath { get; set; }
        [XmlElement("SubTexture")]
        public SubTexture[] items { get; set; }
    }
    [XmlRoot(ElementName = "TextureAtlas")]
    public class SubTexture
    {
        [XmlAttribute]
        public string name { get; set; }
        [XmlAttribute]
        public int x { get; set; }
        [XmlAttribute]
        public int y { get; set; }
        [XmlAttribute]
        public int width { get; set; }
        [XmlAttribute]
        public int height { get; set; }
        [XmlAttribute]
        public int frameX { get; set; }
        [XmlAttribute]
        public int frameY { get; set; }
        [XmlAttribute]
        public int frameWidth { get; set; }
        [XmlAttribute]
        public int frameHeight { get; set; }
    }
}
