using UnityEngine;
using UnityEditor;

public class ObjectPlacer : MonoBehaviour
{
    [MenuItem("Tools/Place to ground %#F6")]
    private static void PlaceToGround()
    {
        GameObject[] objects = Selection.gameObjects;
        foreach (GameObject obj in objects)
        {
            PlaceObject(obj);
        }
    }
    private static void PlaceObject(GameObject obj)
    {
        Collider2D[] colliders = obj.GetComponents<Collider2D>();
        if (colliders.Length == 0)
        {
            return;
        }

        float bottomDelta = float.MaxValue;
        foreach (var col in colliders)
        {
            Vector3 colliderPosition = col.bounds.center;
            Vector3 colliderSize = col.bounds.size;
            float colliderBottom = colliderPosition.y - colliderSize.y * 0.5f;
            float objBottomDelta = colliderBottom - obj.transform.position.y;
            if (objBottomDelta < bottomDelta)
            {
                bottomDelta = objBottomDelta;
            }
        }

        Vector3 raycastPosition = obj.transform.position;
        raycastPosition.y += bottomDelta - 0.01f;

        RaycastHit2D hit = Physics2D.Raycast(raycastPosition, Vector2.down);
        if (hit.collider != null)
        {
            Vector3 pos = hit.point;
            pos.y -= bottomDelta;
            obj.transform.position = pos;
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
