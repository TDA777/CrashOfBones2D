
using UnityEngine;
using UnityEditor;

public class ObjectCloner : EditorWindow
{

    public static string imagePath;
    Object SelObj = null;
    UnityEngine.Object[] SelObjInspector;
    string Width_frame = "", Height_frame = "";
    float pivotX = 0.5f, pivotY = 0.5f;
    bool isFit = false;
    public string TextureSize
    {
        get;
        private set;
    }
    public Object SelectedObject { get; private set; }

    // Add menu item named "My Window" to the Window menu
    [MenuItem("Tools/Window ObjectCloner")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        GetWindow<ObjectCloner>("Object Cloner");
    }
    void OnGUI()
    {
        SelObj = Selection.activeObject;
        if (!SelObj)
        {
            UnityEngine.Object[] SelObjInspector = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
            if (SelObjInspector.Length > 0)
            {
                SelObj = SelObjInspector[0];
            }
        }
        GUILayout.Label("Selection", EditorStyles.boldLabel);
        EditorGUILayout.ObjectField(SelObj, typeof(GameObject), true);
        int selected = 0;
        string[] options = new string[]
        {
            "Option1", "Option2", "Option3",
        };
        selected = EditorGUILayout.Popup("Label", selected, options);
        //if (SelObj)
        //{
        //    imagePath = AssetDatabase.GetAssetPath(SelObj);
        //    TextureSize = (SelObj as Texture2D).width.ToString() + " x " + (SelObj as Texture2D).height.ToString();
        //}
        //else
        //{
        //    imagePath = "";
        //    TextureSize = "";

        //}
        //GUILayout.Label("Image path", EditorStyles.boldLabel);
        //GUILayout.Label(imagePath, EditorStyles.helpBox);

        //GUILayout.Label("Texture size", EditorStyles.boldLabel);
        //GUILayout.Label(TextureSize, EditorStyles.helpBox);

        //GUILayout.Label("Frame size", EditorStyles.boldLabel);
        //Width_frame = EditorGUILayout.TextField("Width frame", Width_frame);
        //Height_frame = EditorGUILayout.TextField("Height frame", Height_frame);

        //pivotX = EditorGUILayout.Slider("Pivot X", pivotX, 0, 1);
        //pivotY = EditorGUILayout.Slider("Pivot Y", pivotY, 0, 1);

        //isFit = EditorGUILayout.Toggle("Fit frame size to size texture", isFit);


        //if (GUILayout.Button("Read"))
        //{
        //    if (!SelObj)
        //    {
        //        Debug.LogWarning("Select texture!");
        //        return;
        //    }
        //    int x, y;
        //    int.TryParse(Width_frame, out x);
        //    int.TryParse(Height_frame, out y);
        //    if (x == 0 || y == 0)
        //    {
        //        Debug.LogWarning("Size frame not a number!");
        //        return;
        //    }

        //    XML_Loader.LoadXML_BySize(imagePath, new Vector2(x, y), new Vector2(pivotX, pivotY), isFit);
        //}
        //GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        //myString = EditorGUILayout.TextField("Text Field", myString);


        //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        //myBool = EditorGUILayout.Toggle("Toggle", myBool);
        //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        //EditorGUILayout.EndToggleGroup();
    }
}
