using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Reflection;
using UnityEditorInternal;

public class LayoutSort : MonoBehaviour
{
    public const string LAYER_NAME = "Default";
    private static List<Transform> sprites;
    [MenuItem("Tools/Reset Layout Sort")]
    private static void LayoutUnSorting()
    {
        Sort();
        string[] layers = GetSortingLayerNames();
        foreach (Transform root in sprites)
        {
            SpriteRenderer[] any = root.GetComponentsInChildren<SpriteRenderer>();

            foreach (SpriteRenderer item in any)
            {
                item.sortingLayerName = layers[0];
            }
        }
    }

    [MenuItem("Tools/Layout Sort %#F7")]
    private static void LayoutSorting()
    {
        Sort();
        string[] layers = GetSortingLayerNames();
        foreach (Transform root in sprites)
        {
            SpriteRenderer[] any = root.GetComponentsInChildren<SpriteRenderer>();

            foreach (SpriteRenderer item in any)
            {
                if (item.GetComponent<Animator>())
                {
                    item.sortingLayerName = layers[3];
                }

            }
        }
    }

    private static void Sort()
    {
        object[] objs = GameObject.FindObjectsOfType(typeof(GameObject));
        sprites = new List<Transform>();
        foreach (GameObject obj in objs)
        {
            Transform trans = obj.gameObject.transform.root;
            Component[] any = trans.GetComponentsInChildren<SpriteRenderer>();
            if (any.Length == 0)
            {
                continue;
            }
            if (!sprites.Contains(trans))
            {
                sprites.Add(trans);
            }
        }
        print(sprites.Count);

    }

    // Get the sorting layer names
    public static string[] GetSortingLayerNames()
    {
        Type internalEditorUtilityType = typeof(InternalEditorUtility);
        PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
        return (string[])sortingLayersProperty.GetValue(null, new object[0]);
    }
    // Get the unique sorting layer IDs -- tossed this in for good measure
    public static int[] GetSortingLayerUniqueIDs()
    {
        Type internalEditorUtilityType = typeof(InternalEditorUtility);
        PropertyInfo sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty("sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);
        return (int[])sortingLayerUniqueIDsProperty.GetValue(null, new object[0]);
    }
}
