using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Experimental.UIElements;
using System.IO;

public class Window : EditorWindow {
    
    public static string imagePath;
    Object image = null;
    string Width_frame = "", Height_frame = "";
    float pivotX = 0.5f, pivotY = 0.5f;
    float scaleY = 0.5f;
    float scaleX = 0.5f;
    bool isFit = false;
    public string TextureSize
    {
        get;
        private set;
    }

    // Add menu item named "My Window" to the Window menu
    [MenuItem("Tools/Window XML_Loader")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        GetWindow<Window>("XML_Loader");
    }
    void OnGUI()
    {
        GUILayout.Label("Offset Y pivot", EditorStyles.boldLabel);
        scaleY = EditorGUILayout.Slider(scaleY, -3, 3);
        GUILayout.Label("Offset X pivot", EditorStyles.boldLabel);
        scaleX = EditorGUILayout.Slider(scaleX, -3, 3);
        if (GUILayout.Button("LoadXML"))
        {
            XML_Loader.OffsetPivotY = scaleY;
            XML_Loader.OffsetPivotX = scaleX;
            XML_Loader.LoadXML();
        }

        GUILayout.Label("", EditorStyles.boldLabel);
        GUILayout.Label("Texture2D", EditorStyles.boldLabel);
        image = EditorGUILayout.ObjectField(image, typeof(Texture2D), true);
        if (image)
        {
            imagePath = AssetDatabase.GetAssetPath(image);
            TextureSize = (image as Texture2D).width.ToString() + " x " + (image as Texture2D).height.ToString();
        }
        else
        {
            imagePath = "";
            TextureSize = "";

        }
        GUILayout.Label("Image path", EditorStyles.boldLabel);
        GUILayout.Label(imagePath, EditorStyles.helpBox);
        
        GUILayout.Label("Texture size", EditorStyles.boldLabel);
        GUILayout.Label(TextureSize, EditorStyles.helpBox);

        GUILayout.Label("Frame size", EditorStyles.boldLabel);
        Width_frame = EditorGUILayout.TextField("Width frame", Width_frame);
        Height_frame = EditorGUILayout.TextField("Height frame", Height_frame);

        pivotX = EditorGUILayout.Slider("Pivot X", pivotX, 0, 1);
        pivotY = EditorGUILayout.Slider("Pivot Y", pivotY, 0, 1);

        isFit = EditorGUILayout.Toggle("Fit frame size to size texture", isFit);
        

        if (GUILayout.Button("Read") )
        {
            if (!image)
            {
                Debug.LogWarning("Select texture!");
                return;
            }
            int x, y;
            int.TryParse(Width_frame, out x);
            int.TryParse(Height_frame, out y);
            if (x == 0 || y == 0)
            {
                Debug.LogWarning("Size frame not a number!");
                return;
            }

            XML_Loader.LoadXML_BySize(imagePath,new Vector2(x,y),new Vector2(pivotX,pivotY), isFit);
        }
        //GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        //myString = EditorGUILayout.TextField("Text Field", myString);


        //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        //myBool = EditorGUILayout.Toggle("Toggle", myBool);
        //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        //EditorGUILayout.EndToggleGroup();
    }
}
;