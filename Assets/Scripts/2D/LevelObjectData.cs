﻿using UnityEngine;

[CreateAssetMenu(fileName ="Data", menuName = "Objects/LevelObjects", order = 1)]
public class LevelObjectData : ScriptableObject
{
    public string Name = "New Level object";
    public bool IsStatic;
    public int Health = 1;
}
