﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : AIBehaviour
{
    private enum MovementType
    {
        linear,
        pingpong
    }

    [SerializeField]
    private MovementType m_MovementType;
    [SerializeField]
    private float m_MaxDistance = 6f;

    private float movementDirection = 1;
    Vector2 startPoint;

    public override void StartBehaviour(AIObject target)
    {
        startPoint = transform.position;
        StartCoroutine(MovementProcess(target.MoveMent, target.Transform));
    }

    private IEnumerator MovementProcess(CharacterMovement movement, Transform forward)
    {
        while (true)
        {
            if(IsPossibleToMove(forward.position, forward.right))
            {
                movement.Move(transform.right * movementDirection);
            }
            else
            {
                movement.Move(Vector2.zero);
            }
            yield return null;
        }
    }

    private bool IsPossibleToMove(Vector2 position, Vector2 direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, direction, 1f);

        if(hit.collider && hit.transform.root.gameObject.GetComponent<IPlayer>() != null)
        {
            return false;
        }

        if (hit.collider != null)
        {
            movementDirection *= -1;
            return true;
        }

        switch (m_MovementType)
        {
            case MovementType.linear:
                return true;

            case MovementType.pingpong:
                if(Mathf.Abs(startPoint.x - transform.position.x) > m_MaxDistance)
                {
                    movementDirection *= -1;
                    return true;
                }
                return true;

            default:
                return false;
        }
    }
}
